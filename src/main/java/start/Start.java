package start;

import tasks.*;

import java.util.List;

import objects.MonitoredData;

public class Start
{
	public static void main(String args[]) 
    {
		Task1 task1 = new Task1();
		Task2 task2 = new Task2();
		Task3 task3 = new Task3();
		Task4 task4 = new Task4();
		Task5 task5 = new Task5();
		Task6 task6 = new Task6();
		
		String inputFile = "Activities.txt";
		task1.task1(inputFile);
		task2.task2(inputFile);
		task3.task3(inputFile);
		task4.task4(inputFile);
		task5.task5(inputFile);
		task6.task6(inputFile);
    }
}
