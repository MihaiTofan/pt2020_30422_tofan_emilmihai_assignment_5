package objects;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MonitoredData
{
	public LocalDateTime startTime;
	public LocalDateTime endTime;
	public String activityLabel;
	
	
	public MonitoredData(LocalDateTime start, LocalDateTime end, String activity)
	{
		this.startTime = start;
		this.endTime = end;
		this.activityLabel = activity;
	}


	public LocalDateTime getStartTime()
	{
		return startTime;
	}


	public void setStartTime(LocalDateTime startTime) 
	{
		this.startTime = startTime;
	}


	public LocalDateTime getEndTime() 
	{
		return endTime;
	}


	public void setEndTime(LocalDateTime endTime) 
	{
		this.endTime = endTime;
	}


	public String getActivityLabel()
	{
		return activityLabel;
	}


	public void setActivityLabel(String activityLabel)
	{
		this.activityLabel = activityLabel;
	}
	
	public String toString()
	{
		String tab = "\t\t";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		return this.startTime.format(formatter) + tab + this.endTime.format(formatter) + tab + this.activityLabel;
	}
	
	

}
