package tasks;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import objects.MonitoredData;

public class Task3
{
	public Task3()
	{
		
	}
	
	public void refreshFile(String outputFile) throws IOException
	{
		PrintWriter pw = new PrintWriter(new FileWriter(outputFile,false));
		pw.write("");
		pw.close();
	}
	
	
	public void task3(String inputFile)
	{
		
		try 
		{
			refreshFile("Task_3.txt");
		}
		catch (IOException e)
		{
			System.out.println("Could not refresh the file Task_3.txt");
		}
		Task1 task = new Task1();
		List<MonitoredData> allElements = task.getElements(inputFile);
		Map<String, Long> map = new HashMap<>();
		try 
		{
			PrintWriter pw = new PrintWriter(new FileWriter("Task_3.txt", true));
			pw.println("\t  Activity \t Number of occurences");
			pw.println("----------------------------------------------");
			
			
			map = allElements
            .stream()
            .collect(Collectors.groupingBy(activity -> activity.activityLabel, Collectors.counting()));
	
			for (String key : map.keySet())
			{
				pw.format("%15s \t %-20s", key, map.get(key));
				pw.println();
				pw.println();
			}
			
			pw.close();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}	
	}
}
