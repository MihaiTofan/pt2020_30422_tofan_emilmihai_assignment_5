package tasks;
import java.time.LocalDateTime;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import objects.MonitoredData;

public class Task1
{
	List<MonitoredData> list;
	
	
	public Task1()
	{
		
	}
	
	
	public List<MonitoredData> getElements(String inputFile)
	{
		List<MonitoredData> allElements = null ;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		
		try (Stream<String> stream = Files.lines(Paths.get(inputFile))) 
		{

			allElements = 
			stream
			.map(string -> string.split("\t\t"))
			.map(words -> new MonitoredData(LocalDateTime.parse(words[0], formatter), LocalDateTime.parse(words[1], formatter), words[2]))
			.collect(Collectors.toList());
			
		} 
		
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return allElements;
		
	}
	
	public void refreshFile(String outputFile) throws IOException
	{
		PrintWriter pw = new PrintWriter(new FileWriter(outputFile,false));
		pw.write("");
		pw.close();
	}
	
	public void task1(String inputFile)
	{
		try 
		{
			refreshFile("Task_1.txt");
		}
		catch (IOException e)
		{
			System.out.println("Could not refresh the file Task_1.txt");
		}
		Task1 task = new Task1();
		List<MonitoredData> allElements = task.getElements(inputFile);
		try 
		{
			PrintWriter pw = new PrintWriter(new FileWriter("Task_1.txt", true));
			allElements
			.stream()
			.forEach(element -> pw.println(element)); 
			pw.close();
			
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

 
}
