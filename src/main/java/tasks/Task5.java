package tasks;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.Duration;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import objects.MonitoredData;

public class Task5 
{

	public Task5()
	{
		
	}
	
	public void refreshFile(String outputFile) throws IOException
	{
		PrintWriter pw = new PrintWriter(new FileWriter(outputFile,false));
		pw.write("");
		pw.close();
	}
	
	public String getHours(int totalSeconds)
    {
    	int hours = totalSeconds / 3600;
    	int minutes = (totalSeconds % 3600) / 60;
    	int seconds = totalSeconds % 60;

    	String timeString = String.format("%02d hours:%02d minutes:%02d seconds", hours, minutes, seconds);
    	return timeString;
    }
	
	public void task5(String inputFile)
	{
		try 
		{
			refreshFile("Task_5.txt");
		}
		catch (IOException e)
		{
			System.out.println("Could not refresh the file Task_5.txt");
		}
		
		Task1 task = new Task1();
		Task5 task5 = new Task5();
		List<MonitoredData> allElements = task.getElements(inputFile);
		Map<String,Integer> map = new HashMap<>();
		
		try 
		{
			PrintWriter pw = new PrintWriter(new FileWriter("Task_5.txt", true));
			pw.println("\t  Activity \t\t\tTotal duration in hours");
			pw.println("------------------------------------------------------------------------");
			
			map = allElements
			.stream()
			.collect(Collectors.groupingBy(activity -> activity.activityLabel,
					 Collectors.summingInt(seconds -> (int)Duration.between(seconds.startTime,seconds.endTime).getSeconds())));
			
			for (String key : map.keySet())
			{
				pw.format("%15s \t\t %-20s", key, task5.getHours(map.get(key)));
				pw.println();
				pw.println();
			}
			
			pw.close();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
}
