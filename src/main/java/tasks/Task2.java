package tasks;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;


import objects.MonitoredData;

public class Task2
{
	
	public Task2()
	{
		
	}
	
	
	public void refreshFile(String outputFile) throws IOException
	{
		PrintWriter pw = new PrintWriter(new FileWriter(outputFile,false));
		pw.write("");
		pw.close();
	}
	
	public void task2(String inputFile)
	{
		try 
		{
			refreshFile("Task_2.txt");
		}
		catch (IOException e)
		{
			System.out.println("Could not refresh the file Task_2.txt");
		}
		Task1 task = new Task1();
		List<MonitoredData> allElements = task.getElements(inputFile);
		try 
		{
			PrintWriter pw = new PrintWriter(new FileWriter("Task_2.txt", true));
			pw.print("The number of distinct days that appear in the monitoring data is : ");
			pw.println(
					allElements
					.stream()
					.map(element -> element.getStartTime().getDayOfYear())
					.distinct()
					.count());
			pw.close();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
				
	}
	
}
