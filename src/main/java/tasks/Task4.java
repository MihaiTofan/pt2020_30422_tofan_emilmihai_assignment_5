package tasks;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import objects.MonitoredData;

public class Task4
{
	public Task4()
	{
		
	}
	
	public void refreshFile(String outputFile) throws IOException
	{
		PrintWriter pw = new PrintWriter(new FileWriter(outputFile,false));
		pw.write("");
		pw.close();
	}
	
	public void task4(String inputFile)
	{
		try 
		{
			refreshFile("Task_4.txt");
		}
		catch (IOException e)
		{
			System.out.println("Could not refresh the file Task_4.txt");
		}
		
		Task1 task = new Task1();
		List<MonitoredData> allElements = task.getElements(inputFile);
		Map<Integer, Map<String, Long>> map = new HashMap<>();
		
		try 
		{
			PrintWriter pw = new PrintWriter(new FileWriter("Task_4.txt", true));
			map = allElements
			.stream()
			.collect(Collectors.groupingBy(day -> day.getStartTime().getDayOfYear(),
					 Collectors.groupingBy(activity -> activity.activityLabel, Collectors.counting())));
			
			int count = 1;
			for (int key : map.keySet()) 
			{
				//THE NUMBER OF THE MONITORED DAY -> 28 NOVEMBER = DAY 1
				
				pw.format("Day %d activities: ",count++ );
				pw.print(map.get(key));
				pw.println();
				
			}
			pw.close();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
	
	}
}
