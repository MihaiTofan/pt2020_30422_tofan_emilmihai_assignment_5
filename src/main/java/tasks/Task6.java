package tasks;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import objects.MonitoredData;

public class Task6
{
	public Task6()
	{
		
	}
	
	public void refreshFile(String outputFile) throws IOException
	{
		PrintWriter pw = new PrintWriter(new FileWriter(outputFile,false));
		pw.write("");
		pw.close();
	}
	
	public void task6(String inputFile)
	{
		try 
		{
			refreshFile("Task_6.txt");
		}
		catch (IOException e)
		{
			System.out.println("Could not refresh the file Task_6.txt");
		}
		
		Task1 task = new Task1();
		List<MonitoredData> allElements = task.getElements(inputFile);
	    List<MonitoredData> smallDurationElements;
		List<String> results = new ArrayList<String>() ;
		try 
		{
			PrintWriter pw = new PrintWriter(new FileWriter("Task_6.txt", true));
				smallDurationElements = allElements
				.stream()
				.filter(element -> Duration.between(element.startTime,element.endTime).getSeconds() < 300)
				.collect(Collectors.toList());
			
				Map<String, Long> allElementsMap = allElements
	            .stream()
	            .collect(Collectors.groupingBy(activity -> activity.activityLabel, Collectors.counting()));
				
				Map<String, Long> smallDurationMap = smallDurationElements
				.stream()
	            .collect(Collectors.groupingBy(activity -> activity.activityLabel, Collectors.counting()));	

				results = smallDurationMap.keySet()
				.stream()
				.filter(element -> smallDurationMap.get(element) > allElementsMap.get(element) *0.9)
				.collect(Collectors.toList());
				
				for(int i = 0; i < results.size(); i++)
				{
					pw.println("The list of  activities that have more than 90% of the monitoring records with duration\r\n" + 
							"less than 5 minutes is : ");
					pw.println(results.get(i));
				}
				
		
			pw.close();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
			
		}
	}
}
